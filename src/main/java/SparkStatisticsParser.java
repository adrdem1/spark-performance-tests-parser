import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class SparkStatisticsParser {

    private static Logger logger = LoggerFactory.getLogger(SparkStatisticsParser.class);

    public static void main(String[] args) throws IOException {

        String host;

        if (args.length != 0){
            host = args[0];
        } else{
            host = "localhost";
        }

        String sparkStreamingUiUrl = "http://" + host + ":30012/streaming/";
        logger.info("Connecting to {}", sparkStreamingUiUrl);
        Document doc = Jsoup.connect(sparkStreamingUiUrl).get();
        logger.debug("The parsed web page is: {}", doc);
        Element table = doc.select("table#completed-batches-table").get(0);
        Elements rows = table.select("tbody").select("tr");

        /**
         * Stores the summatory of processing times
         */
        double processingTimeSum = 0;
        /**
         * Stores the number of valid baches, i.e. batches processing records
         */
        int validProcessedBatchesNumber = 0;
        /**
         * Stores indices of rows containing valid batches
         */
        List<Integer> batchesToProcessIndicesList = new ArrayList<>();

        for (int i = 0; i < rows.size(); i++) {
            Element row = rows.get(i);
            Elements cols = row.select("td");
            int processedRecords = Integer.parseInt(cols.get(1).text().replaceAll("\\D+", ""));
            if (processedRecords != 0) { // Filter batches with 0 record processed
                batchesToProcessIndicesList.add(i);
            } else{
                logger.debug("Discarding row {}. Processed records: {}", i, processedRecords);
            }
        }

        for (int i = 0; i < batchesToProcessIndicesList.size() - 1; i++) { // We skip the oldest batch
            logger.debug("---- ROW {} ----", batchesToProcessIndicesList.get(i));
            Element row = rows.get(batchesToProcessIndicesList.get(i));
            Elements cols = row.select("td");
            validProcessedBatchesNumber++;
            String processingTimeString = cols.get(3).text();
            if (processingTimeString.contains("s")) { // If processing time is in seconds
                int processingTime = Integer.parseInt(processingTimeString.replaceAll("[^\\.0123456789]", ""));
                processingTimeSum += processingTime;
                logger.debug("Added {} as {} seconds", processingTimeString, processingTime);
            } else if (processingTimeString.contains("min")) { // If processing time is in minutes
                double processingTime = Double.parseDouble(processingTimeString.replaceAll("[^\\.0123456789]", ""));
                processingTimeSum += processingTime * 60;
                logger.debug("Added {} as {} seconds", processingTimeString, processingTime * 60);
            }
        }

        logger.info("The summatory of processing time is {} seconds", processingTimeSum);
        logger.info("There have been {} valid processed batches", validProcessedBatchesNumber);

        System.out.println("The average processed time has been " + processingTimeSum / validProcessedBatchesNumber + " seconds");
    }
}
